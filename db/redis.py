from typing import Optional

from redis import Redis

from db.base import Database


class RedisDB(Database):
    scheme = 'redis'

    def connect(self):
        self.redis = Redis.from_url(self._url)

    def get(self, shortened_url: str) -> Optional[str]:
        return self.redis.get(shortened_url)

    def set(self, shortened_url: str, redirect: str):
        self.redis.set(shortened_url, redirect)


# TODO: not finished - just example how to scale redis horizontally for this case
class RedisFarmDB(Database):
    FARM_SIZE = 10

    def get_redis(self, shortened_url: str) -> Redis:
        return self.redis[shortened_url.__hash__() % self.FARM_SIZE]

    def connect(self):
        self.redis = [Redis.from_url(f'redis://redis-instance-{i}' for i in range(self.FARM_SIZE))]

    def get(self, shortened_url: str) -> Optional[str]:
        return self.get_redis(shortened_url).get(shortened_url)

    def set(self, shortened_url: str, redirect: str):
        return self.get_redis(shortened_url).set(shortened_url, redirect)
