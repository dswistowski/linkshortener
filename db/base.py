import abc
from functools import lru_cache
from typing import Optional
from urllib import parse

import settings


class Database(abc.ABC):
    scheme = ''

    def __init__(self, url: str) -> None:
        assert self.scheme != ''
        self._url = url
        self.connect()

    @abc.abstractmethod
    def connect(self):
        pass

    @abc.abstractmethod
    def get(self, shortened_url: str) -> Optional[str]:
        pass

    @abc.abstractmethod
    def set(self, shortened_url: str, redirect: str):
        pass


@lru_cache()
def get_all_databases():
    # TODO: load them dynamically
    from db.redis import RedisDB
    all_backends = {RedisDB}
    return {backend.scheme: backend for backend in all_backends}


@lru_cache()
def get_database(database=settings.DATABASE) -> Database:
    backends = get_all_databases()
    parsed = parse.urlsplit(database)
    if parsed.scheme not in backends:
        raise RuntimeError(f'Unknown database backend: {parsed.scheme}')
    return backends[parsed.scheme](database)
