import random
import string
from typing import Optional

import settings
from db.base import get_database

ALPHABET = string.digits + string.ascii_letters


def gen_id(alphabet=ALPHABET, length=8) -> str:
    return ''.join(random.choice(alphabet) for _ in range(length))


def shorten_url(url: str) -> str:
    db = get_database()
    shortened = gen_id()
    # collision?
    # TODO: it's not acid at the moment
    while db.get(shortened):
        shortened = gen_id()
    db.set(shortened, url)
    return f'https://{settings.HOSTNAME}/{shortened}'


def get_redirect(key: str) -> Optional[str]:
    db = get_database()
    return db.get(key)
