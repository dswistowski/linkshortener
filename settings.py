import os

HOSTNAME = os.environ.get('HOSTNAME', 'dswistowski-linkshortener-dev.herokuapp.com')
DATABASE = os.environ.get('DATABASE', os.environ.get('REDIS_URL', 'redis://localhost'))
