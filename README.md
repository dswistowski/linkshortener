# linkshortener

## Running local tests:

1. Run local redis
   
   It's possible to use normal, or docker redis. 
  
2. Install requirements

   `python -m pip install pipenv && pipenv install && pipenv install -d`
3. Run the tests

   `python -m pytest`
   
## Testing deployed version

  master branch is deployed under https://dswistowski-linkshortener-dev.herokuapp.com/  url.
  
  Shortering url can be done with:
  
  ```
  $ http POST https://dswistowski-linkshortener-dev.herokuapp.com/shorten_url url=http://wp.pl
  ```
  
  Testing redirection with:
  ```
  http GET https://dswistowski-linkshortener-dev.herokuapp.com/oHyhwKj5
  ```
  
## Scalling webservice

 This webservice is 12 factor - so it can be scaled horizontally. There is no caching - because fast key value database is used as the backend. 
 
 Redis backend can be easily scaled as well, example is presented in`RedisFarmDB` class.
 
 