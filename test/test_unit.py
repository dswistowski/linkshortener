from backend import gen_id
from db.base import get_database
from db.redis import RedisDB


def test_gen_id():
    for i in range(40):
        assert gen_id('AB', 2) in {'AA', 'AB', 'BA', 'BB'}


def test_can_write_to_database(redis_db: RedisDB):
    redis_db.set('foo', 'bar')
    assert redis_db.redis.set.called_with('foo', 'bar')


def test_can_read_from_database(redis_db: RedisDB):
    redis_db.redis.get.return_value = 'bar'

    assert redis_db.get('foo') == 'bar'


def test_get_database_will_return_redis_db_for_redis_url():
    db = get_database('redis://localhost')
    assert isinstance(db, RedisDB)
