from unittest.mock import MagicMock

import pytest

import settings
from db.redis import RedisDB

settings.HOSTNAME = 'www.your-service.com'
settings.DATABASE = 'redis://redis'


@pytest.fixture
def app():
    from app import create_app
    app = create_app()
    return app


@pytest.fixture
def redis():
    return MagicMock()


@pytest.fixture
def redis_db(redis):
    redisdb = RedisDB('redis://localhost')
    redisdb.redis = redis
    return redisdb
