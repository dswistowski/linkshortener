from typing import Dict, NamedTuple

import pytest
from flask.testing import FlaskClient


def test_should_return_ok_on_root(client: FlaskClient):
    result = client.get('/')
    assert result.status_code == 200
    assert result.data == b'OK'


def test_should_return_shortened_url(client: FlaskClient):
    result = client.post('/shorten_url', json={
        'url': 'http://www.helloworld.com'
    })
    assert result.status_code == 201
    assert result.json['shortened_url'].startswith('https://www.your-service.com/')


def test_shorten_url_endpoint_should_not_break_on_nonjson_request(client: FlaskClient):
    result = client.post('/shorten_url', data='foo-bar')
    assert result.status_code == 400


@pytest.mark.parametrize("json", [
    {'foo': 'bar'},
    {'url': {'foo': 'bar'}},
    None,
    2,
    {'url': True},
    {'url': None}
])
def test_shorten_url_endpoint_should_not_break_on_wrong_json_data(client: FlaskClient, json: Dict):
    result = client.post('/shorten_url', json=json)
    assert result.status_code == 400


def test_wrong_url_will_return_404(client: FlaskClient):
    result = client.get('/foobar')
    assert result.status_code == 404


class UrlFixture(NamedTuple):
    url: str
    shortened_url: str


@pytest.fixture
def url_fixture(client: FlaskClient) -> UrlFixture:
    url = 'https://www.hello-world.co.uk/foo/bar?dssad=1'
    response = client.post('/shorten_url', json={'url': url})
    return UrlFixture(
        url=url,
        shortened_url=response.json['shortened_url'].split('/')[-1]
    )


def test_can_go_to_already_shortened_url(client: FlaskClient, url_fixture: UrlFixture):
    result = client.get(f'/{url_fixture.shortened_url}')
    assert result.status_code == 302
