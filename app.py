from flask import Flask, request, jsonify, make_response, redirect, abort

import backend


def error_response(error):
    return make_response(jsonify({'error': error}), 400)


class ValidationError(Exception):
    pass


def validate_content(content):
    if not isinstance(content, dict):
        raise ValidationError('Given json is in wrong format, object with key `url` is expected')
    if 'url' not in content:
        raise ValidationError('Given json do not have `url` key')
    url = content['url']
    if not isinstance(url, str):
        raise ValidationError('Given url is not a string')
    if not url.startswith('http://') and not url.startswith('https://'):
        raise ValidationError('Only http or https urls are allowed')
    return url


def create_app() -> Flask:
    app = Flask(__name__)

    @app.route('/')
    def status_page():
        return 'OK'

    @app.route('/shorten_url', methods=['POST'])
    def shorten_url():
        if not request.is_json:
            return error_response('Request is not a json')
        content = request.get_json(silent=True)
        try:
            url = validate_content(content)
        except ValidationError as e:
            return error_response(e.args[0])

        shortened_url = backend.shorten_url(url)
        return make_response(jsonify({
            'shortened_url': shortened_url
        }), 201)

    @app.route('/<string:url>')
    def do_redirect(url):
        redirect_url = backend.get_redirect(url)
        if redirect_url:
            return redirect(redirect_url, code=302)
        abort(404)

    return app
